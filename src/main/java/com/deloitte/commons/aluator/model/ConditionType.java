/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.model;

/**
 * Standard condition types.
 *
 * @author pcornish
 */
public class ConditionType {
    public static final String IS_EMPTY = "IS_EMPTY";
    public static final String IS_NOT_EMPTY = "IS_NOT_EMPTY";
    public static final String IS_EQUAL_TO = "IS_EQUAL_TO";
    public static final String IS_NOT_EQUAL_TO = "IS_NOT_EQUAL_TO";

    /**
     * Don't allow instantiation.
     */
    private ConditionType() {
    }
}
