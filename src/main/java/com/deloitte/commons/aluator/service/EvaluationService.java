/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.service;

import com.deloitte.commons.aluator.condition.Condition;
import com.deloitte.commons.aluator.extractor.ValueExtractor;
import com.deloitte.commons.aluator.model.EvaluationConfig;
import com.deloitte.commons.aluator.model.EvaluationResult;
import com.deloitte.commons.aluator.model.ValueHolder;

import java.util.Map;

/**
 * Evaluates a config against facts.
 *
 * @author pcornish
 */
public interface EvaluationService {
    /**
     * Evaluate the config against the facts.
     * <p/>
     * If there are no facts, then the result is always <code>false</code>.
     *
     * @param facts  the facts against which to evaluate the config
     * @param config the config to evaluate against the facts
     * @return <code>true</code> if the config evaluates positively against the facts, otherwise <code>false</code>
     */
    boolean evaluate(Map<String, Object> facts, EvaluationConfig config);

    /**
     * Evaluate the config against the facts.
     * <p/>
     * If there are no facts, then the result is always <code>false</code>.
     *
     * @param facts  the facts against which to evaluate the config
     * @param config the config to evaluate against the facts
     * @return a the result of the evaluation
     */
    EvaluationResult evaluateForResult(Map<String, Object> facts, EvaluationConfig config);

    /**
     * Evaluate the config against the facts.
     * <p/>
     * If there are no facts, then the result is always <code>false</code>.
     *
     * @param facts  the facts against which to evaluate the config
     * @param config the config to evaluate against the facts
     * @return <code>true</code> if the config evaluates positively against the facts, otherwise <code>false</code>
     */
    boolean evaluateValueHolders(Map<String, ? extends ValueHolder> facts, EvaluationConfig config);

    /**
     * Register a condition with the given name so that it can be used in the evaluation config.
     *
     * @param conditionType the unique name of the condition type
     * @param condition     the Condition implementation
     */
    void registerCondition(String conditionType, Condition condition);

    /**
     * Register a ValueExtractor for the given Class.
     *
     * @param classBinding   the unique Class to which the ValueExtractor is bound (note: subclasses also checked)
     * @param valueExtractor the ValueExtractor to bind to the given Class
     * @param <T>            the type of value extracted
     */
    <T> void registerValueExtractor(Class<T> classBinding, ValueExtractor<T> valueExtractor);
}
