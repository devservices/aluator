/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.extractor;

/**
 * Extracts a value from a fact.
 *
 * @author pcornish
 */
public interface ValueExtractor<T> {
    /**
     * Extract the value from the fact.
     *
     * @param fact the fact from which to extract the value. This will never be <code>null</code>
     * @return the value of the fact
     */
    Object extractValue(T fact);
}
