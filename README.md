# aluator - A simple validation framework for Java.

## Overview
A simple validation framework for Java, where JSR-303 is too heavyweight or you need to validate collections of POJOs.

This is particularly useful on Android, where JSR-303 implementations are quite heavyweight.

Configuration driven by JSON config, e.g.

    {
        "ruleName": "User name and email address cannot both be empty",
        "message": "Please fill in user name or email address",
        "operator": "OR",
        "conditions": [
            {
                "objectName": "userName",
                "conditionType": "IS_NOT_EMPTY"
            },
            {
                "objectName": "emailAddress",
                "conditionType": "IS_NOT_EMPTY"
            }
        ]
    }
    
API is very simple:

    final EvaluationConfig config = // set config or load config from file
    
    // set up your facts
    final Map<String, Object> facts = new HashMap<>();
    facts.put("userName", userName);
    facts.put("emailAddress", emailAddress);
    
    // evaluate facts
    final boolean outcome = service.evaluate(facts, config);
        
## Build
### Pre-requisites
Install Java 6+.

### Compile and package
Build the project using the Gradle wrapper in the root directory.

    ./gradlew build
    
This will compile the source code, run the tests and, if successful, build a JAR file under the *build* folder.

## License
This project is released under the Apache 2.0 License.

## Authors
* Pete Cornish (pcornish@deloitte.co.uk)
