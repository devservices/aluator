/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.condition;

import com.deloitte.commons.aluator.model.ConditionConfig;

/**
 * Evaluates to <code>true</code> if the nested {@link Condition} is <code>false</code>.
 *
 * @author pcornish
 */
public class InvertedCondition implements Condition {
    private Condition originalCondition;

    public InvertedCondition(Condition originalCondition) {
        this.originalCondition = originalCondition;
    }

    @Override
    public boolean matches(ConditionConfig condition, Object fact, Object objectValue) {
        return !originalCondition.matches(condition, fact, objectValue);
    }
}
