/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.model;

import java.util.List;

/**
 * A ruleset for validation.
 *
 * @author pcornish
 */
public class EvaluationConfig {
    /**
     * A description of the validation ruleset.
     */
    private String description;

    /**
     * The validation rules.
     */
    private List<RuleConfig> rules;

    /**
     * Whether to stop evaluating on failure.
     */
    private boolean haltOnFailure;

    /**
     * Invert a rule match result so that a match is a failure and vice versa.
     */
    private boolean ruleMatchIsFailure;

    public String getDescription() {
        return description;
    }

    public List<RuleConfig> getRules() {
        return rules;
    }

    public boolean isHaltOnFailure() {
        return haltOnFailure;
    }

    public void setHaltOnFailure(boolean haltOnFailure) {
        this.haltOnFailure = haltOnFailure;
    }

    public boolean isRuleMatchIsFailure() {
        return ruleMatchIsFailure;
    }
}
