/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.condition;

import com.deloitte.commons.aluator.model.ConditionConfig;

/**
 * Represents a condition.
 *
 * @author pcornish
 */
public interface Condition {
    /**
     * Evaluate the condition on the object value.
     *
     * @param condition   the condition to evaluate against the object
     * @param fact        the fact from which the value was extracted
     * @param objectValue the value of the object to evaluate
     * @return <code>true</code> if the evaluation was positive for the object, otherwise <code>false</code>
     */
    boolean matches(ConditionConfig condition, Object fact, Object objectValue);
}
