/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.service;

import com.deloitte.commons.aluator.condition.Condition;
import com.deloitte.commons.aluator.condition.EmptyCondition;
import com.deloitte.commons.aluator.condition.EqualsCondition;
import com.deloitte.commons.aluator.condition.InvertedCondition;
import com.deloitte.commons.aluator.extractor.ValueExtractor;
import com.deloitte.commons.aluator.extractor.ValueHolderValueExtractor;
import com.deloitte.commons.aluator.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Evaluates a config against facts.
 *
 * @author pcornish
 */
public class EvaluationServiceImpl implements EvaluationService {
    private Map<String, Condition> conditions = new HashMap<String, Condition>();
    private Map<Class<?>, ValueExtractor> valueExtractors = new HashMap<Class<?>, ValueExtractor>();

    /**
     * Whether to evaluate ValueExtractor subclasses. This is slower, but more flexible.
     */
    private boolean evaluateValueExtractorSubclasses = true;

    public EvaluationServiceImpl() {
        // default conditions
        final EqualsCondition equalsCondition = new EqualsCondition();
        conditions.put(ConditionType.IS_EQUAL_TO, equalsCondition);
        conditions.put(ConditionType.IS_NOT_EQUAL_TO, new InvertedCondition(equalsCondition));

        final EmptyCondition emptyCondition = new EmptyCondition();
        conditions.put(ConditionType.IS_EMPTY, emptyCondition);
        conditions.put(ConditionType.IS_NOT_EMPTY, new InvertedCondition(emptyCondition));

        // default value extractors
        valueExtractors.put(ValueHolder.class, new ValueHolderValueExtractor());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean evaluate(Map<String, Object> facts, EvaluationConfig config) {
        return evaluateForResult(facts, config).isSuccess();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EvaluationResult evaluateForResult(Map<String, Object> facts, EvaluationConfig config) {
        final EvaluationResult result = new EvaluationResult();
        result.setMessages(new ArrayList<String>());
        result.setSuccess(true);

        for (RuleConfig rule : config.getRules()) {
            boolean ruleResult = evaluateRule(facts, rule);
            if (config.isRuleMatchIsFailure()) {
                ruleResult = !ruleResult;
            }

            if (!ruleResult) {
                result.setSuccess(false);

                // add failure message
                result.getMessages().add(rule.getMessage());

                // fail fast
                if (config.isHaltOnFailure()) {
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Evaluate the given rule against the facts.
     *
     * @param facts                   the facts to evaluate
     * @param rule                    the rules to apply
     * @return <code>true</code> if the evaluation was positive for the rule against all facts, otherwise <code>false</code>
     */
    private boolean evaluateRule(Map<String, Object> facts, RuleConfig rule) {
        // short circuit
        if (facts.size() == 0) {
            return false;
        }

        boolean ruleOutcome;
        switch (rule.getOperator()) {
            case AND:
                // true util a condition is false
                ruleOutcome = true;
                break;

            case OR:
                // false until a condition is true
                ruleOutcome = false;
                break;

            default:
                throw new UnsupportedOperationException(String.valueOf(rule.getOperator()));
        }

        for (ConditionConfig condition : rule.getConditions()) {
            final Object fact = facts.get(condition.getObjectName());

            boolean conditionOutcome = evaluateCondition(condition, fact);

            if (EvaluationOperator.AND.equals(rule.getOperator())) {
                // failure
                if (!conditionOutcome) {
                    ruleOutcome = false;
                    break;
                }

            } else if (EvaluationOperator.OR.equals(rule.getOperator())) {
                // success
                if (conditionOutcome) {
                    ruleOutcome = true;
                    break;
                }
            }
        }

        return ruleOutcome;
    }

    /**
     * Evaluate the condition on the object value, using the appropriate
     * {@link com.deloitte.commons.aluator.condition.Condition}.
     *
     * @param conditionConfig the condition to evaluate against the object
     * @param fact            the value of the object to evaluate
     * @return <code>true</code> if the evaluation was positive for the object, otherwise <code>false</code>
     */
    private boolean evaluateCondition(ConditionConfig conditionConfig, Object fact) {
        final Condition condition = conditions.get(conditionConfig.getConditionType());
        if (null == condition) {
            throw new UnsupportedOperationException("Unknown condition type: " + conditionConfig.getConditionType());
        }

        final Object factValue = extractValue(fact);
        return condition.matches(conditionConfig, fact, factValue);
    }

    /**
     * Extract the value from the fact, using a {@link com.deloitte.commons.aluator.extractor.ValueExtractor},
     * if one is configured, otherwise return the fact as the value itself.
     *
     * @param fact the fact from which to extract the value
     * @return the value of the fact
     */
    @SuppressWarnings("unchecked")
    private Object extractValue(Object fact) {
        if (null == fact) {
            return null;
        }

        ValueExtractor valueExtractor = valueExtractors.get(fact.getClass());

        // check if fact is a subclass (slower)
        if (null == valueExtractor && evaluateValueExtractorSubclasses) {
            for (Class<?> clazz : valueExtractors.keySet()) {
                if (clazz.isAssignableFrom(fact.getClass())) {
                    valueExtractor = valueExtractors.get(clazz);
                    break;
                }
            }
        }

        // the fact is the value
        if (null == valueExtractor) {
            return fact;
        }

        return valueExtractor.extractValue(fact);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean evaluateValueHolders(Map<String, ? extends ValueHolder> facts, EvaluationConfig config) {
        final Map<String, Object> objectFacts = new HashMap<String, Object>();
        objectFacts.putAll(facts);
        return evaluate(objectFacts, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerCondition(String conditionType, Condition condition) {
        conditions.put(conditionType, condition);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> void registerValueExtractor(Class<T> classBinding, ValueExtractor<T> valueExtractor) {
        valueExtractors.put(classBinding, valueExtractor);
    }

    /**
     * @param evaluateValueExtractorSubclasses whether to evaluate ValueExtractor subclasses.
     *                                         This is slower, but more flexible.
     */
    public void setEvaluateValueExtractorSubclasses(boolean evaluateValueExtractorSubclasses) {
        this.evaluateValueExtractorSubclasses = evaluateValueExtractorSubclasses;
    }
}
