/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.test.service;

import com.deloitte.commons.aluator.model.EvaluationConfig;
import com.deloitte.commons.aluator.model.EvaluationResult;
import com.deloitte.commons.aluator.service.EvaluationServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Tests for {@link com.deloitte.commons.aluator.service.EvaluationServiceImpl} that verify the EvaluationResult.
 *
 * @author pcornish
 */
public class EvaluationServiceTest_CheckResult {
    /**
     * Service under test.
     */
    private EvaluationServiceImpl service;
    private Gson gson;

    @Before
    public void before() {
        service = new EvaluationServiceImpl();
        gson = new Gson();
    }

    @Test
    public void testEvaluateForResult_False_SingleRule() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "foo");
        facts.put("B", "bar");

        final EvaluationConfig config = loadConfigFile("/empty/example1.json");

        // call
        final EvaluationResult actual = service.evaluateForResult(facts, config);

        // assert output
        assertNotNull("Evaluation result should be returned", actual);
        assertFalse("Evaluation should be negative", actual.isSuccess());
        assertNotNull("Validation messages should be populated", actual.getMessages());
        assertEquals("Validation messages should be populated", 1, actual.getMessages().size());
        assertEquals("Validation message should be populated", "Please fill A and B", actual.getMessages().get(0));
    }

    @Test
    public void testEvaluateForResult_False_MultipleRules() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "foo");
        facts.put("B", "bar");

        final EvaluationConfig config = loadConfigFile("/empty/example3.json");

        // call
        final EvaluationResult actual = service.evaluateForResult(facts, config);

        // assert output
        assertNotNull("Evaluation result should be returned", actual);
        assertFalse("Evaluation should be negative", actual.isSuccess());
        assertNotNull("Validation messages should be populated", actual.getMessages());
        assertEquals("Validation messages should be populated", 2, actual.getMessages().size());
        assertEquals("Validation message should be populated", "Please fill A and B", actual.getMessages().get(0));
        assertEquals("Validation message should be populated", "Please fill A and B", actual.getMessages().get(1));
    }

    @Test
    public void testEvaluateForResult_False_MultipleRules_HaltOnFailure() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "foo");
        facts.put("B", "bar");

        final EvaluationConfig config = loadConfigFile("/empty/example3.json");
        config.setHaltOnFailure(true);

        // call
        final EvaluationResult actual = service.evaluateForResult(facts, config);

        // assert output
        assertNotNull("Evaluation result should be returned", actual);
        assertFalse("Evaluation should be negative", actual.isSuccess());
        assertNotNull("Validation messages should be populated", actual.getMessages());
        assertEquals("Validation messages should be populated", 1, actual.getMessages().size());
        assertEquals("Validation message should be populated", "Please fill A and B", actual.getMessages().get(0));
    }

    @Test
    public void testEvaluateForResult_False_ConditionMatchIsFailure() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "");
        facts.put("B", "");

        final EvaluationConfig config = loadConfigFile("/empty/example5.json");
        config.setHaltOnFailure(true);

        // call
        final EvaluationResult actual = service.evaluateForResult(facts, config);

        // assert output
        assertNotNull("Evaluation result should be returned", actual);
        assertFalse("Evaluation should be negative", actual.isSuccess());
        assertNotNull("Validation messages should be populated", actual.getMessages());
        assertEquals("Validation messages should be populated", 1, actual.getMessages().size());
        assertEquals("Validation message should be populated", "Please fill A or B", actual.getMessages().get(0));
    }

    /**
     * Load the configuration from a file on the classpath.
     *
     * @param fileName the name of the config file
     * @return the EvaluationConfig from the file
     * @throws java.io.IOException
     */
    private EvaluationConfig loadConfigFile(String fileName) throws IOException {
        final InputStream jsonStream = EvaluationServiceTest_CheckResult.class.getResourceAsStream(fileName);
        if (null == jsonStream) {
            throw new FileNotFoundException(fileName);
        }

        final String jsonConfig = IOUtils.toString(jsonStream);
        return gson.fromJson(jsonConfig, EvaluationConfig.class);
    }
}
