/*
 * Copyright 2014 Deloitte Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deloitte.commons.aluator.test.service;

import com.deloitte.commons.aluator.model.EvaluationConfig;
import com.deloitte.commons.aluator.service.EvaluationServiceImpl;
import com.deloitte.commons.aluator.test.support.TestValueHolder;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;

/**
 * Tests for {@link com.deloitte.commons.aluator.service.EvaluationServiceImpl} equals conditions
 * that should evaluate to <code>false</code>.
 *
 * @author pcornish
 */
public class EvaluationServiceTest_Equals_Negative {
    /**
     * Service under test.
     */
    private EvaluationServiceImpl service;
    private Gson gson;

    @Before
    public void before() {
        service = new EvaluationServiceImpl();
        gson = new Gson();
    }

    @Test
    public void testEvaluate_False_And_Operator_SingleRule() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "hello");
        facts.put("B", "world");

        final EvaluationConfig config = loadConfigFile("/equals/example1.json");

        // call
        final boolean actual = service.evaluate(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluate_False_And_Operator_MultipleRules() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "hello");
        facts.put("B", "world");

        final EvaluationConfig config = loadConfigFile("/equals/example3.json");

        // call
        final boolean actual = service.evaluate(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluate_False_Or_Operator_SingleRule() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "");
        facts.put("B", "");

        final EvaluationConfig config = loadConfigFile("/equals/example2.json");

        // call
        final boolean actual = service.evaluate(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluate_False_Or_Operator_MultipleRules() throws IOException {
        // test data
        final Map<String, Object> facts = new HashMap<String, Object>();
        facts.put("A", "");
        facts.put("B", "");

        final EvaluationConfig config = loadConfigFile("/equals/example4.json");

        // call
        final boolean actual = service.evaluate(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluateValueHolders_False_And_Operator_SingleRule() throws IOException {
        // test data
        final Map<String, TestValueHolder> facts = new HashMap<String, TestValueHolder>();
        facts.put("A", new TestValueHolder("hello"));
        facts.put("B", new TestValueHolder("world"));

        final EvaluationConfig config = loadConfigFile("/equals/example1.json");

        // call
        final boolean actual = service.evaluateValueHolders(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluateValueHolders_False_And_Operator_MultipleRules() throws IOException {
        // test data
        final Map<String, TestValueHolder> facts = new HashMap<String, TestValueHolder>();
        facts.put("A", new TestValueHolder("hello"));
        facts.put("B", new TestValueHolder("world"));

        final EvaluationConfig config = loadConfigFile("/equals/example3.json");

        // call
        final boolean actual = service.evaluateValueHolders(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluateValueHolders_False_Or_Operator_SingleRule() throws IOException {
        // test data
        final Map<String, TestValueHolder> facts = new HashMap<String, TestValueHolder>();
        facts.put("A", new TestValueHolder(""));
        facts.put("B", new TestValueHolder(""));

        final EvaluationConfig config = loadConfigFile("/equals/example2.json");

        // call
        final boolean actual = service.evaluateValueHolders(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    @Test
    public void testEvaluateValueHolders_False_Or_Operator_MultipleRules() throws IOException {
        // test data
        final Map<String, TestValueHolder> facts = new HashMap<String, TestValueHolder>();
        facts.put("A", new TestValueHolder(""));
        facts.put("B", new TestValueHolder(""));

        final EvaluationConfig config = loadConfigFile("/equals/example4.json");

        // call
        final boolean actual = service.evaluateValueHolders(facts, config);

        // assert output
        assertFalse("Evaluation should be negative", actual);
    }

    /**
     * Load the configuration from a file on the classpath.
     *
     * @param fileName the name of the config file
     * @return the EvaluationConfig from the file
     * @throws java.io.IOException
     */
    private EvaluationConfig loadConfigFile(String fileName) throws IOException {
        final InputStream jsonStream = EvaluationServiceTest_Equals_Negative.class.getResourceAsStream(fileName);
        if (null == jsonStream) {
            throw new FileNotFoundException(fileName);
        }

        final String jsonConfig = IOUtils.toString(jsonStream);
        return gson.fromJson(jsonConfig, EvaluationConfig.class);
    }
}
